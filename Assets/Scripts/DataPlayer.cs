﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Sorcerer,
    Knight,
    Assesin,
    Bard,
    Barbarian
}
public enum State
{
    AutoMove,
    PersonMove
}
public enum Moving
{
    Moving,
    Stop
}
public class DataPlayer : MonoBehaviour
{
    public string FirstName;
    public string LastName;
    public PlayerKind Kind;
    public float Height;
    public float Speed;
    public float Weight;
    public float WalkDistance;
    public Sprite[] Sprites;
    public State Estat;
    public Moving EstatMoviment;
    public int Lifes;

    // Start is called before the first frame update
    void Start()
    {
        Estat = State.AutoMove;
        EstatMoviment = Moving.Moving;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
