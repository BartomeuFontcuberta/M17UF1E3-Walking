﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance{get{return _instance;}}
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public GameObject Player;
    public GameObject Canvas;
    public GameObject Spawner;
    private int _points;
    public int Points { get { return _points; } }
    public int Enemies = 0;

    // Start is called before the first frame update
    void Start()
    {
        _points = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncrasePoints(int increment)
    {
        _points += increment;
    }
    public void EnemyDead(int pointsEarned)
    {
        Enemies--;
        IncrasePoints(pointsEarned);
        Canvas.GetComponent<UIEnemyController>().RechargeEnemies();
        Canvas.GetComponent<UIEnemyController>().RechargePoints();
    }
    public void EnemyAttack()
    {
        Enemies--;
        Player.GetComponent<DataPlayer>().Lifes --;
        Canvas.GetComponent<UIEnemyController>().RechargeEnemies();
        Canvas.GetComponent<UIEnemyController>().RechargeLifes();
    }
    public void EnemyAppears()
    {
        Enemies ++;
        Canvas.GetComponent<UIEnemyController>().RechargeEnemies();
    }
    public void PlayerDead()
    {

        SceneManager.LoadScene("Enemy");
        /*canvas.GetComponent<UIEnemyController>().PlayerDead();
        spawner.SetActive(spawner.activeSelf);*/
    }
}
