﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum HabilityGiantStates
{
    Ground,
    GiantState,
    Unground,
    Colddown,
    WaitToActive
}
public class GiantMode : MonoBehaviour
{
    private Transform grandaria;
    private DataPlayer jugador;
    internal int refredament=0;
    public int espera;
    public int duracio;
    private float alturaBase;
    internal HabilityGiantStates estatHabilitat;
    // Start is called before the first frame update
    void Start()
    {
        estatHabilitat = HabilityGiantStates.WaitToActive;
        grandaria = gameObject.GetComponent<Transform>();
        jugador =gameObject.GetComponent<DataPlayer>();
        alturaBase = jugador.Height;
    }

    // Update is called once per frame
    void Update()
    {
        switch (estatHabilitat)
        {
            case HabilityGiantStates.WaitToActive:
                alturaBase = jugador.Height;
                break;
            case HabilityGiantStates.Ground:
                canviarTamany(0.5f);
                if (grandaria.localScale.x >= 15)
                {
                    refredament = duracio*12;
                    estatHabilitat = HabilityGiantStates.GiantState;
                }
                break;
            case HabilityGiantStates.GiantState:
                refredament--;
                if (refredament == 0)
                {
                    estatHabilitat = HabilityGiantStates.Unground;
                }
                break;
            case HabilityGiantStates.Unground:
                canviarTamany(-0.5f);
                if (grandaria.localScale.x <= 5)
                {
                    refredament = espera*12;
                    estatHabilitat = HabilityGiantStates.Colddown;
                }
                break;
            case HabilityGiantStates.Colddown:
                refredament--;
                if (refredament == 0)
                {
                    estatHabilitat=HabilityGiantStates.WaitToActive;
                }
                break;
        }
    }
    public void canviarTamany(float creixement)
    {
        jugador.Height +=(alturaBase/5)*creixement;
        grandaria.localScale = new Vector3(grandaria.localScale.x+creixement, grandaria.localScale.y+creixement, grandaria.localScale.z);
    }
}
