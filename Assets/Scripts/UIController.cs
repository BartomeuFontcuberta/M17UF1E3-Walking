﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text frames;
    public Text velocitat;
    public Text distancia;
    public Text duracio;
    public Text espera;
    private int numFrames=0;
    public DataPlayer jugador;
    public GiantMode habilitat;
    public Moviment moviment;
    public Text actHabilitat;
    public Text canMode;
    public SpriteRenderer imatge;
    // Start is called before the first frame update
    void Start()
    {
        velocitat.text = ("Velocitat: " + jugador.Speed);
        distancia.text = ("Distància: " + jugador.WalkDistance);
        duracio.text = ("Duració: " + habilitat.duracio);
        espera.text = ("Espera: " + habilitat.espera);
    }

    // Update is called once per frame
    void Update()
    {
        frames.text = ("Frames: "+numFrames.ToString());
        numFrames++;
        switch (habilitat.estatHabilitat)
        {
            case HabilityGiantStates.WaitToActive:
                actHabilitat.text =("H");
                break;
            case HabilityGiantStates.Ground:
                actHabilitat.text = ("^");
                break;
            case HabilityGiantStates.Unground:
                actHabilitat.text = ("v");
                break;
            case HabilityGiantStates.GiantState:
            case HabilityGiantStates.Colddown:
                actHabilitat.text = ((habilitat.refredament/12).ToString());
                break;
        }
    }
    public void canviarVelocitat(bool augmentar)
    {
        if (augmentar)
        {
            jugador.Speed += 5;
        }
        else
        {
            if (jugador.Speed != 5)
            {
                jugador.Speed -= 5;
            }
        }
        velocitat.text = ("Velocitat: "+jugador.Speed);
    }
    public void canviarDistancia(bool augmentar)
    {
        if (augmentar)
        {
            jugador.WalkDistance ++;
        }
        else
        {
            if (jugador.WalkDistance != 1)
            {
                jugador.WalkDistance --;
            }
        }
        distancia.text = ("Distància: " + jugador.WalkDistance);
    }
    public void canviarDuracio(bool augmentar)
    {
        if (augmentar)
        {
             habilitat.duracio++;
        }
        else
        {
            if (habilitat.duracio != 1)
            {
                habilitat.duracio--;
            }
        }
        duracio.text = ("Duració: " + habilitat.duracio);
    }
    public void canviarEspera(bool augmentar)
    {
        if (augmentar)
        {
            habilitat.espera++;
        }
        else
        {
            if (habilitat.espera != 1)
            {
                habilitat.espera--;
            }
        }
        espera.text = ("Espera: " + habilitat.espera);
    }
    public void activarHabilitat()
    {
        switch (habilitat.estatHabilitat)
        {
            case HabilityGiantStates.WaitToActive:
                habilitat.estatHabilitat = HabilityGiantStates.Ground;
                break;
        }
    }
    public void canviMode()
    {
        switch (jugador.Estat)
        {
            case State.AutoMove:
                jugador.Estat = State.PersonMove;
                canMode.text = ("A");
                break;
            case State.PersonMove:
                moviment.sentit = 1;
                imatge.flipX = true;
                jugador.Estat = State.AutoMove;
                jugador.EstatMoviment = Moving.Moving;
                canMode.text = ("M");
                break;
        }
    }
}
