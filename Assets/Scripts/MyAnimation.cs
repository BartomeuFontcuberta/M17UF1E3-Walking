﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private SpriteRenderer spriteRenderer;
    private int animationSpriteIndex = 0;
    private DataPlayer dades;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 12;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        animationSprites = gameObject.GetComponent<DataPlayer>().Sprites;
        dades = gameObject.GetComponent<DataPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (dades.EstatMoviment)
        {
            case Moving.Moving:
                changeSprite();
                break;
            case Moving.Stop:
                stopSprite();
                break;
        }
        
    }
    private void changeSprite()
    {
        spriteRenderer.sprite = animationSprites[animationSpriteIndex];
        animationSpriteIndex++;
        if (animationSpriteIndex == animationSprites.Length)
        {
            animationSpriteIndex = 0;
        }
    }
    private void stopSprite()
    {
        spriteRenderer.sprite = animationSprites[1];
    }
    
}
