﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollitionController : MonoBehaviour
{
    //Collition controler with enemies and player
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //Code to collision with player
            Debug.Log("Colosió amb Player");
            GameObject player = collision.gameObject;
            GameManager.Instance.EnemyAttack();

            if (player.GetComponent<DataPlayer>().Lifes == 0)
            {
                Destroy(collision.gameObject);
                GameManager.Instance.PlayerDead();
            }
            
        }else if (collision.CompareTag("Enemy"))
        {
            GameManager.Instance.EnemyDead(0);
        }
        Destroy(gameObject);
    }
}
