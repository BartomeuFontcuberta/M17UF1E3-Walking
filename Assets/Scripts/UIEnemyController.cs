﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEnemyController : MonoBehaviour
{
    public Text Lifes;
    public Text Enemies;
    public Text Points;
    // Start is called before the first frame update
    void Start()
    {
        RechargeEnemies();
        RechargePoints();
        RechargeLifes();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RechargeLifes()
    {
        Lifes.text =("Vides: "+GameManager.Instance.Player.GetComponent<DataPlayer>().Lifes);
    }
    public void RechargeEnemies()
    {
        Enemies.text = ("Enemics: " + GameManager.Instance.Enemies);
    }
    public void RechargePoints()
    {
        Points.text = ("Punts: " + GameManager.Instance.Points);
    }
    /*public void PlayerDead()
    {
        
    }*/
}
