﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateEnemy",1,2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateEnemy()
    {
        GameManager.Instance.EnemyAppears();
        GameObject enemic;
        if (Mathf.RoundToInt(Random.value)==0)
        {
            enemic=Instantiate(enemy, (Camera.main.ScreenToWorldPoint(new Vector3(costat(Screen.width), Random.Range(0,Screen.height), 0))), Quaternion.identity);
        }
        else
        {
            enemic=Instantiate(enemy, (Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), costat(Screen.height), 0))),Quaternion.identity);
        }
        Transform eT=enemic.GetComponent<Transform>();
        eT.position = new Vector3(eT.position.x,eT.position.y,0);
    }
    private int costat(int oposat)
    {
        if (Mathf.RoundToInt(Random.value) == 0)
        {
            return oposat;
        }
        else
        {
            return 0;
        }
    }
}
