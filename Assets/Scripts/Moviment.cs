﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moviment : MonoBehaviour
{
    private SpriteRenderer imatge;
    private Transform moviment;
    private DataPlayer dades;
    private float objectiu;
    internal int sentit = 1;
    private int espera=0;

    // Start is called before the first frame update
    void Start()
    {
        imatge = gameObject.GetComponent<SpriteRenderer>();
        dades = gameObject.GetComponent<DataPlayer>();
        moviment = gameObject.GetComponent<Transform>();
        objectiu = dades.WalkDistance;
        imatge.flipX = true;
    }

    // Update is called once per frame
    void Update()
    {
        switch (dades.Estat)
        {
            case State.AutoMove:
                vigilancy();
                break;
            case State.PersonMove:
                playerMove();
                break;
        }
    }
    private void playerMove()
    {
        if (Input.GetAxis("Horizontal")>0)
        {
            imatge.flipX = true;
            dades.EstatMoviment = Moving.Moving;
            move(1);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            imatge.flipX = false;
            dades.EstatMoviment = Moving.Moving;
            move(-1);
        }
        else
        {
            dades.EstatMoviment = Moving.Stop;
        }
    }
    private void vigilancy()
    {
        switch (dades.EstatMoviment)
        {
            case Moving.Moving:
                move(sentit);
                objectiu -= dades.Speed / dades.Weight;
                if (objectiu < 0)
                {
                    objectiu = dades.WalkDistance;
                    sentit *= -1;
                    if (sentit == 1)
                    {
                        dades.EstatMoviment = Moving.Stop;
                        espera = 10;
                    }
                    imatge.flipX = !imatge.flipX;
                }
                break;
            case Moving.Stop:
                espera--;
                if (espera == 0)
                {
                    dades.EstatMoviment = Moving.Moving;
                }
                break;
        }
    }
    private void move(int direccio)
    {
        moviment.position = new Vector3(moviment.position.x + ((dades.Speed/dades.Weight) * direccio), moviment.position.y, moviment.position.z);
    }
}
