﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveController : MonoBehaviour
{
    [SerializeField]
    private int scorePoints = 5;
    public Transform transformE;
    public Transform transformP;
    private int direccioX;
    private int direccioY;
    public float error = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        transformP = GameManager.Instance.Player.GetComponent<Transform>();
        transformE =gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transformP != null)
        {
            if (transformP.position.x > transformE.position.x + error)
            {
                direccioX = 1;
            }
            else if (transformP.position.x < transformE.position.x - error)
            {
                direccioX = -1;
            }
            else
            {
                direccioX = 0;
            }
            if (transformP.position.y > transformE.position.y + error)
            {
                direccioY = 1;
            }
            else if (transformP.position.y < transformE.position.y - error)
            {
                direccioY = -1;
            }
            else
            {
                direccioY = 0;
            }
            transform.Translate(Time.deltaTime * direccioX, Time.deltaTime * direccioY, 0);
        }
    }
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0)){
            GameManager.Instance.EnemyDead(scorePoints);
            Destroy(gameObject);
        }
    }
}
